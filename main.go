package main

import (
	"gitlab.com/sanoval.awwalin/users/app"
	"gitlab.com/sanoval.awwalin/users/config"
)

func main() {
	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	app.Run(":" + config.Server.Port)
}
